/**
 * Created by EH on 1/11/2015.
 */
public class MagicSquare {

	public static boolean isSolved (int[][] sq){
		int n = sq.length;
		int checkSum = ((n*n*n)+n)/2;
		int rowSum = 0, colSum = 0, diag1Sum = 0, diag2Sum = 0;

		for (int number = 1; number <= n*n; number++) {
			// Check if each number appears exactly once
			int count = 0;
			for (int i = 0; i < n; i++){
				if(sq[i].length != n) return false;
				for (int j = 0; j < n; j++)
					if(sq[i][j] == number) count++;
			}
			if (count != 1) return false;
		}

		for (int i = 0; i < n; i++) {
			rowSum = 0;
			for (int j = 0; j < n; j++)
				rowSum += sq[i][j];
			if (rowSum != checkSum) return false;
		}

		for (int i = 0; i < n; i++) {
			colSum = 0;
			for (int j = 0; j < n; j++)
				colSum += sq[j][i];
			if (colSum != checkSum) return false;
		}

		for (int i = 0; i < n; i++)
			diag1Sum += sq[i][i];

		for (int i = n - 1; i >= 0; i--)
			diag2Sum += sq[i][i];

		if (diag1Sum != checkSum || diag2Sum != checkSum) return false;

		return true;
	}

	public static int[][] MagicSquare(int n) {
		int[][] sq = new int[n][n];
		if(solve(sq,0,new boolean[n*n])) return sq;

		return new int[][] {};
	}

	private static boolean solve(int[][] sq, int pos, boolean[] used) {
		int n = sq.length;
		int col = pos % n;
		int row = (pos - col) / n;

		// Basisfall
		if (isSolved(sq)) return true;
		if (pos == n*n) return false;

		// Rekursion
		for (int i = 0; i < used.length; i++){
			if (used[i]) continue;
			sq[row][col] = i + 1;
			used[i] = true;
			if (solve(sq,pos + 1, used)) return true;
			used[i] = false;
		}

		return false;
	}

}
