import org.junit.Test;

import static org.junit.Assert.*;

public class MagicSquareTest {

	@Test
	public void checkMagicSquare3() {
		// Arrange
		int[][] square = new int[][]{
				{2,7,6},
				{9,5,1},
				{4,3,8}
		};


		// Act
		assertTrue(MagicSquare.isSolved(square));
	}

	@Test
	public void checkMagicSquare4() {
		// Arrange
		int[][] square = new int[][]{
				{1,15,14,4},
				{12,6,7,9},
				{8,10,11,5},
				{13,3,2,16}
		};

		// Act
		assertTrue(MagicSquare.isSolved(square));
	}

	@Test
	public void creates3() {
		// Assert
		int count = 3;

		// Act
		int[][] actual = MagicSquare.MagicSquare(count);

		// Assert
		assertEquals(count, actual.length);
		assertTrue(MagicSquare.isSolved(actual));
	}

	@Test
	public void creates4() {
		// Assert
		int count = 4;

		// Act
		int[][] actual = MagicSquare.MagicSquare(count);

		// Assert
		assertEquals(count, actual.length);
		assertTrue(MagicSquare.isSolved(actual));
	}



}